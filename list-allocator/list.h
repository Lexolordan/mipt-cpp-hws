#pragma once

#include <list>

#include "allocator.h"

namespace mipt {
// Your code goes here

template<typename T>
using List = std::list<T, StackAllocator<T, 100'000>>;

} // namespace mipt
